package com.epam.rd.java.basic.task7.db;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.epam.rd.java.basic.task7.db.entity.*;
import org.apache.ibatis.jdbc.ScriptRunner;

public class DBManager {

    private static DBManager instance;
    private static Properties properties;

    private static final String URL;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        } else return instance;
        return instance;
    }

    private DBManager() {
    }

    static {
        String url = "";
        try (InputStream inputStream = new FileInputStream("app.properties")) {
            properties = new Properties();
            properties.load(inputStream);
            url = properties.getProperty("connection.url");
        } catch (Exception e) {
            e.printStackTrace();
        }
        URL = url;
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(URL);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM users");
            while (resultSet.next()) {
                users.add(getUserFrom(resultSet));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        String query = "INSERT INTO users VALUES (DEFAULT, ?)";
        try(Connection connection = DriverManager.getConnection(URL);
                PreparedStatement preparedStatement = connection.prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS);) {
            preparedStatement.setString(1, user.getLogin());
            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int fieldID = resultSet.getInt(1);
                user.setId(fieldID);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return true;
    }
//
    public boolean deleteUsers(User... users) throws DBException {
                boolean result = true;
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     con.prepareStatement("DELETE FROM users WHERE login=?")) {
            //int k = 1;
            for (User u : users) {
                statement.setString(1, u.getLogin());
                result = result && statement.executeUpdate() > 0;
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return result;
    }

    public User getUser(String login) throws DBException {
        User user = null;
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("SELECT id, login FROM users where login = ?")) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                user = getUserFrom(resultSet);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return user;
    }

    public Team getTeam(String name) throws DBException {
        Team team = null;
        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement = connection.prepareStatement("SELECT id, name FROM teams WHERE name=?")) {
            statement.setString(1, name);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                team = getTeamFrom(resultSet);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return team;
    }

    public List<Team> findAllTeams() throws DBException {
        List<Team> teams = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection(URL);
            if (connection != null) {
                Statement statement = connection.createStatement();
                ResultSet resultSet = statement.executeQuery("SELECT * FROM teams");
                while (resultSet.next()) {
                    teams.add(getTeamFrom(resultSet));
                }
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        String query = "INSERT INTO teams VALUES (DEFAULT, ?)";

        try(Connection connection = DriverManager.getConnection(URL);
                PreparedStatement preparedStatement = connection.prepareStatement(query,
                Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setString(1, team.getName());
            if (preparedStatement.executeUpdate() != 1) {
                return false;
            }
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next()) {
                int fieldID = resultSet.getInt(1);
                team.setId(fieldID);
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return true;
    }

    public boolean deleteTeam(Team team) throws DBException {
        boolean result;

        try (Connection connection = DriverManager.getConnection(URL);
             PreparedStatement statement =
                     connection.prepareStatement("DELETE FROM teams WHERE name=?")) {
            statement.setString(1, team.getName());
            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return result;
    }
//
    public boolean updateTeam(Team team) throws DBException {
        boolean result;

        try {
            Connection con = DriverManager.getConnection(URL);
            PreparedStatement statement =
                    con.prepareStatement("UPDATE teams SET name=? WHERE id=?");

            int k = 1;
            statement.setString(k++, team.getName());
            statement.setInt(k, team.getId());

            result = statement.executeUpdate() > 0;
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return result;

    }
    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        Connection con = null;
        try {
            con = DriverManager.getConnection(URL);
            con.setAutoCommit(false);
            con.setTransactionIsolation(Connection.TRANSACTION_READ_COMMITTED);
            for (Team t : teams) {
                setTeamForUserCheck(con, user, t);
            }
            con.commit();
        } catch (SQLException ex) {
            if (con != null) {
                try {
                    con.rollback();
                } catch (SQLException e) {
                    throw new DBException(e.getMessage(), e.getCause());
                }
            }
        }
        return true;
    }

    private static boolean setTeamForUserCheck(Connection con, User user, Team team) throws SQLException {
        try {
            setTeamForUser(con, user, team);
        } catch (DBException ex) {
            con.rollback();
            con.close();
            return false;
        }
        return true;
    }

    private static void setTeamForUser(Connection con, User user, Team team) throws DBException {
        try (PreparedStatement statement =
                     con.prepareStatement("INSERT INTO users_teams VALUES (?, ?)")) {
            //int k = 1;
            statement.setInt(1, user.getId());
            statement.setInt(2, team.getId());
            statement.executeUpdate();

        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
    }


    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teams = new ArrayList<>();
        ResultSet rs;
        String SQL_GET_USER_TEAMS = "SELECT * FROM teams WHERE teams.id = ANY (SELECT team_id FROM users_teams WHERE user_id=?)";
        try (Connection con = DriverManager.getConnection(URL);
             PreparedStatement statement = con.prepareStatement(SQL_GET_USER_TEAMS)) {

            statement.setInt(1, user.getId());
            rs = statement.executeQuery();
            while (rs.next()) {
                teams.add(getTeamFrom(rs));
            }
        } catch (SQLException e) {
            throw new DBException(e.getMessage(), e.getCause());
        }
        return teams;
    }

    private Team getTeamFrom(ResultSet rs) throws SQLException {
        Team team = new Team();
        team.setId(rs.getInt("id"));
        team.setName(rs.getString("name"));
        return team;
    }
    private User getUserFrom(ResultSet rs) throws SQLException {
        User user = new User();
        user.setId(rs.getInt("id"));
        user.setLogin(rs.getString("login"));
        return user;
    }
}
